echo $HUGO_VERSION
echo $HUGO_ARCHIVE

# install utilities
apk update --no-cache 
apk add --virtual utils \
wget \

# download hugo 
wget --quiet https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_ARCHIVE}.tar.gz -O /tmp/${HUGO_ARCHIVE}.tar.gz
mkdir /usr/local/hugo
echo /tmp/${HUGO_ARCHIVE}.tar.gz
tar xzf /tmp/${HUGO_ARCHIVE}.tar.gz -C /usr/local/hugo/
ln -s /usr/local/hugo/hugo /usr/local/bin/hugo
rm /tmp/${HUGO_ARCHIVE}.tar.gz
hugo version